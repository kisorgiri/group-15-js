// // callback

// // callback is a function which is passed as an argument into another function

// var a = 'sdklfj';
// a = 444;
// a = true
// a = [];
// a = {};
// a = function() {
//     console.log('i am calle dwhen name is called');
// }

// function welcome(name) { // higer order function
//     // higer order function are those function who accepts function as an argument
//     console.log('what comes in >>', name);
//     name();
// }

// a();

// welcome(function() {

// });

function askMoney(amt, ram) {

    console.log('i asked money with mom');
    console.log('mom told me to wait for few hours');
    setTimeout(function() {
        console.log('mum have money now');
        ram(amt + 1000); /// time lagne case ma return keyword kaam gardaina
    }, 2000)
}

askMoney(2000, function() {
    console.log('result of ask money handled')
    console.log('go to buy mobile');

});
console.log('eat food');
console.log('take bath');
// console.log('result >>>', result);

// callback is a result handling mechanism for asynchronus time consuming tasks

// task

// prepare a story to buy laptop
// step 1 go to shop
// step 2 shopkeeper told to wait till evening because he dont have desired model
// step 3 non-blocking kaam(eat food)
// start coding when you have laptop