// string, number,boolean,null,undefined
// array and object

var str = 'brodway';
var phone = 333;
var presentStatus = true;

// array
// array is a data type use to hold multiple values
// the value of array is called element of array
// element inside array always have index
// syntax  two way of declaring array
// 1st constructor syntax
// var hobbies = new Array('hi');

// bracket notation syntax
var intrests = ['coding', 'cyling', 44, true];
// intrests[0] // coding
console.log('array >>', intrests[10]); // true
// intrests[333] // undefined
// object
// object collection of key value pair

// syntax of writing object
// var obj = new Object();
// constructor syntax

// bracket notation
var ram = {
        name: 'ram',
        address: 'tinkune',
        rollNo: 33,
        presentStatus: true,
        phone: 4444
    }
    // accessing property of object
    // two way of accessing property of object
    // 1st dot notation
console.log('name property >>', ram.helicopter); // ram;
// ram.address // tinkune
// ram.abc // undefined



// naming convention
// PascalCase = EmailAddress
// camelCase = emailAddress
// snake_case = email_address
// skewer-case = email-address

// JAVASCRIPT is loosely typed programming language
var name;
name = 'slkdjf';
name = 44;
name = true;
name = ['hi'];
name = {
    abc: 'zyx'
}

console.log('name is >>', name);

//
// ECMAscript 