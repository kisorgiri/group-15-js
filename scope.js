// // scope scope is accessibility of application data
// // varibales constant and function
// // var hi = 'hello';

// // function welcome(name) {
// //     var inner = 'hi i am welcome';

// // }

// // function sayHello() {
// //     var hi = 'hello i am sayHello';
// // }
// // console.log('hi >>', hi);

// // sayHello()
// // global scope // hi welcome, sayhello

// // localscope name ,inner

// // let and var
// // let is es6 feature of declaring varibales
// // var is also used for declaring varibales
// // var maintains functional(local scope)
// // let maintains block scope


// var test = 'welcome';

// function sayHello(name) {
//     let innerTest = 'hi';
//     if (name) { //
//         // truthy falsy property
//         // defined value are truthy value
//         // undefined value are falsy value '',null,undefined,NaN, false
//         let innerTest = 'name exist';

//     } else {

//         let innerTest = 'name doesnot exist';
//     }
//     console.log('innerTest is >>>>', innerTest);

// }
// // console.log('innerTest outside >>', innerTest);
// sayHello(true);
let welcome = 'djlf'
function hi() {
     welcome = 'welcome to broadway';
    // variables that are declared without using var or let are global variables
}
// hi();

function welcome() {
    "use strict"
    test = 'hi';
}
welcome();

console.log('welcome >>>', welcome);