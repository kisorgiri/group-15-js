// promise is an object
// {key -value pair data}
// eg {name:function(){}}
// promise is an object which holds future result

// promise has 3 state
// pending state
// onFulfilled State(success)
// onRejection State(Failure)

// settled

// promise will never change its state once it is either rejectted or fullfilled


// promise has 3 method(execution part)
// askForMoney() assume it as promise 
// 	.then() (method to handle both success and failure) but ideally we use it for success
// 	.catch() method to handle failure
// 	.finally() method that specify promise is settled

// promise syntax

var askMoney = new Promise(function(success, failure) {
    // 1st argument or success is for success callback
    // 2nd argument or failure is for failure (error) callback
    //  1st arugment must be called if success
    // 2nd argument must be called failure
    // what ever is called first result is handled and never can be changed
    setTimeout(function() {
        // success('3333');
        failure('no-money');
    }, 3000);
});
console.log('execution start');
askMoney
    .then(function(data) {
        console.log('data >>>', data);
    }) // then is used for success 
    .catch(function(err) {
        console.log('error >>', err);
    }) // failure
    .finally(function() {
    	console.log('i am finally settled')
    });

console.log('non blocking task');