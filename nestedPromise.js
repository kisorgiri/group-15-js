function askForNote(topic) {
    var res = new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve('yes note');
        }, 2000);
    });
    return res;
}

function servicing() {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            reject('servicing');
        }, 1000);
    });
}

function entertainment() {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            reject('entertainment');
        }, 2000);
    })
}

askForNote('js')
    .then(function(data) {
        console.log('result of askForNote');
        console.log('i have note now');
// 
        var entRes =  entertainment();
        console.log('non blocking work for entertainment');
        return entRes;


    })
    .then(function(data) {
        console.log('success of entertainment >>', data);

        return servicing();
    })

    .then(function(data) {

        console.log('result of servicing');
        // return 'hello';
    }) 
    .catch(function(err) {
        console.log('error in Promise', err);
    })
console.log('wash clothes');