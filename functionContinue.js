// // function with argument

// function welcome(name) { // name is placeholder
//     // JAVASCRIPT is loosely typed programming language
//     console.log('what comes in >>>', name);

// }

// welcome('ram')
// welcome(33)
// welcome(true)
// welcome([true])
// welcome({})
// welcome();

// function sendWelcomeMail(username,name, email,addr,phone) {
// 	// if(!name){
// 	// 	name  = 'default value'
// 	// }
//     var msgBody = 'Hi ' + name + ', Welcome to Broadway';
//     console.log('msg body >>', msgBody);
//     console.log('email address is >>', email);
// }

// sendWelcomeMail(null,'ram@gmail.com','bkt' )
// sendWelcomeMail(null, null,null,3333)
// sendWelcomeMail('hari',null, 'hari@gmail.com',333)

// sendWelcomeMail(null,'bipin@gmail.com');

// function with multiple argument
var hi ='lsdkfj';
function sendWelcomeMail(details) {
    // if(!name){
    // 	name  = 'default value'
    // }
    var msgBody = 'Hi ' + details.name + ', Welcome to Broadway';
    console.log('msg body >>', msgBody);
    console.log('email address is >>', details.email);
}

sendWelcomeMail({
    name: 'ram',
    phone: 'edd',
    address: 'djf'
})
sendWelcomeMail({
    name: 'shyam',
    email: 'sdkfj',
    phone: 'edd',
})
sendWelcomeMail({
    email: 'sdkfj',
    phone: 'edd',
    address: 'djf'
})

// function with return type

function fetchWater(amount) {
	var ram = 'hello';
    // business logic
    // console.log('inside')

    // // return 'hi from return type function'; // loosely typed

    // var data = 'hi';
    // console.log('data >>>', data);
    // return {
    //     name: 'Broadway',
    //     address: 'tinkune'
    // }
}

var result = fetchWater();
console.log('what  comes out >>', result);

// function and scope
// scope accessibility and visibility
// type of scope
// global scope
// functional scope(local scope)
// block scope
