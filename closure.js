// closure 
// closure is a inner function that has access to
// 1.global scope
// 2.own scope
// 3.own arguments
// 4. parent function scope
// 5 parent function argument

var test = 'welcome';

function welcome(name) {

    var inner = 'to';

    function innerFn(location) { // this is closure
        var hi = 'hi';
        var text = hi + ' ' + name + ', ' +
            test + ' ' + inner + ' ' + location;
        return text;
    }

    function sayHello() {
        return 'hey i am sayHello';
    }
    // hi // not accessible
    // var innerResult = innerFn('butwal');
    // console.log('innerResult', innerResult);
    function setName(newName) {
        inner = newName;
    }

    return {
        first: innerFn,
        second: sayHello,
        third: 'sdlkjf',
        setName: setName
    }
}
// inner//  not accessbile
// innerFn // not accessible
// welcome('ramesh');

var result = welcome('ram');
console.log('result  1>>>', result.setName('hello'));
console.log('sentence >>',result.first("tinkune"));
// console.log('result  2>>>', result.second());

// 