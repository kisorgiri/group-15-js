// function
// function is reusable block of code  that performs specific task

// if js { } this will determine the block of code
// {}
// if(condition){
// // if block
// }
// else{
// 	// else block
// }

// function syntax two way of writing a function
//  function (){
// 
// }
// 1 expression syntax
// 2 declarative sytax
var a;

function welcome() {

}
// 1 expression syntax
// b();
var b = function() { // variable declaraion
    console.log('i am inside function a');

}

// welcome();

// declarative syntax
function welcome() {
    console.log('i am welcom function');
}

//Hoisting
// Hoisting is a mechanism where all the declaration are moved at top before execution.

// type of function
// 1 named function
// 2 unnamed function (anynamous function)
// 3 function with argument
// 4 function with return type
// 5 function with argument and return type
// 6 IIFE 
// 1 named function , a,welcome 
// 2 unammed function
// function() {
// unammed function  anynamous function
// }
// IIFE immediately invoked functional expression

// syntax
// (welcome)();
// IIFE using anynamous function
// (function(){
// 	console.log('i am anynamous function and IIFE block')
// })();
