// var fruits = ['apple', 'banana', 'apple', 'orange', 'apple', 'kiwi', 'banana', 'orange', 'apple3', 'banana', 'orange'];
// // index

// // inbuild methods
// //indexOf// index of item in an array
// console.log('index of apple >>', fruits.indexOf('mangos'));
// console.log('last index of apple >>', fruits.lastIndexOf('kiwis'));

// // length
// console.log('fruits length ?>>',fruits.length)

// var cars = ['honda'];
// // insertin into arrays
// // 1 at first
// // 2 at last
// // somewhere in between
// // add at first
// cars.unshift('bajaj');
// console.log('cars >>>', cars);
// // at last
// console.log('cars length before push >>', cars.length);
// cars.push('cbz');
// var newL = cars.push('cbz1'); // add and return length of array
// console.log('new length >>', newL);
// console.log('cars now >>', cars);

// // removeing item from arrays
// // 1 from first,from last and from between somewhere
// // remove from first
// var firstItem = cars.shift(); // remove and retrun frist item from array
// console.log('first item >>', firstItem);
// console.log('cars after shift >>', cars);

// // from last of array
// var lastItem = cars.pop();
// console.log('last item >>>', lastItem);

// console.log('cars after pop >>>', cars);

// // splice // splice is method that is used add or remove items in an array
// // remove using splice
// var indexOfMaruti = cars.indexOf('maruti');
// cars.splice(indexOfMaruti, 1); // remove using splice
// cars.splice(cars.indexOf('tata'), 0, 'ford') // ;add using splice
// console.log('cars after splice >>', cars);

// // splice to add and remvoe
// cars.splice(cars.indexOf('ferrari'), 2, 'a', 'b', 'c');
// console.log('cars >>', cars);

// loop in arrays
// for in 
// forEach
// filter
// map
// reduce
// some
// every


// forEach
// var cars = ['bmw', 'ferrari', 'maruti', 'tata'];

// cars.forEach(function(item, i) {
//     console.log('i will be invoked for each element in  any array');
//     console.log('item is >>', item);
//     console.log('index is >>>', i)
// });

var phones = [{
    name: 's10',
    brand: 'samsung',
    color: 'silver',
    price: '20',
    ram: '8gb'
}, {
    name: 'i phone',
    brand: 'apple',
    color: 'white',
    price: '25',
    ram: '6gb'
}, {
    name: 'one plus',
    brand: 'one plus',
    color: 'black',
    price: '30',
    ram: '8gb'
}, {
    name: 'nokia 6',
    brand: 'nokia',
    color: 'black',
    price: '30',
    ram: '6gb'
}, {
    name: 'redmi',
    brand: 'xaomi',
    color: 'silver',
    price: '20',
    ram: '8gb'
}]

console.log('phones is >>', phones);
phones.forEach(function(item, i) {
    console.log('item is >>>', item.name);
    item.status = 'available';
})
console.log('phones is now >>', phones);

//filter // return array
var a = [];
var eightGbRamPhones = phones.filter(function(item, i) {
    if (item.ram == '8gb') {
        return true;
    }
});
console.log('g gb ram phones >>', eightGbRamPhones);
var coloredPhone = eightGbRamPhones.filter(function(item, i) {
    if (item.color == 'black') {
        return item;
    }
});
console.log('colored phone >>', coloredPhone);
console.log('phones >>>', phones);

phones.map(function(item, i) {
    if (item.color == 'black' && item.ram == '8gb') {
        item.status = 'sold';
    }
});
console.log('phones >>>', phones);

phones.forEach(function(item, i) {
    if (item.status == 'sold') {
        phones.splice(i, 1);
    }
})
console.log('phones after splcie >>>', phones);

// type conversion
// array to string

var vegitables = ['potato', 'pumpkin', 'tomato'];
console.log('chekc array >>', Array.isArray('vegitables'))
if (vegitables.length) {
    console.log('item exist in array');
}

// var toArray = vegitables.join(' ');
// console.log('to array >>', toArray);

//









// var obj = {
//     name: 'brodway',
// };
// console.log('obj is >>',obj);c
// obj.address = 'tinkune' // adding property in object
// console.log('obj is now >>',obj)
// obj.address // accessing property of object