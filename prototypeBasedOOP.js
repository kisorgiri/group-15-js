// functional constructor is an basic building block for prototype based oop

// syntax
console.log('global this >>', this);

function Fruits() {
    // this.color = 'yellow';
    // this.abcd = 'hi';
    // this is a contructor everything assigned to this will be carried for all instance
    console.log('this inside function ', this);
}
Fruits.prototype.origin = 'nepal';
Fruits.prototype.abcd = 'hello';
Fruits.prototype.sayHello = function() {
    return 'from prototype';
}
var apple = new Fruits();
var banana = new Fruits();
console.log('apple is >>>', apple.abcd);
console.log('banana is >>>', banana.sayHello());

// Array
// Object
// String
// Number
var a = new Array();
console.log('a >>',a);
console.log('a length ',a.length);

// prototype keyword is used to add property and method for functional constructor