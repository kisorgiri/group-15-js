
var http = require('http');
var fileOp = require('./file');

var server = http.createServer(function (request, response) {
    // this function will be executed whenever a client is connected
    // request or 1st argument is http request object
    // response or 2nd argument is http response object
    var url = request.url;
    if (url == '/write') {
        fileOp.write('abcd.js', 'hi')
            .then(function (data) {
                response.end('data', data);
            })
            .catch(function (err) {
                response.end(err);
            })
    } else if (url == '/read') {
        fileOp.readFile('abcd.js', function (err, done) {
            if (err) {
                response.end(err);
            } else {
                response.end('done' + done);
            }
        })
    } else {
        response.end('form default page');
    }

    console.log('client connected to server');
    console.log('request url >>', request.url);
    // request response cycle must be completed
    // response.end('hi from node server'); response cannot sent more than once

});

server.listen(9090, function (err, done) {
    if (err) {
        console.log('server listening failed');
    }
    else {
        console.log('server listening at port 9090');
        console.log('press CTRL + C to exit from server');
    }
});


// HTTP is stateless protocol..
