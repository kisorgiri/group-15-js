var fs = require('fs');

// write

function write(fileName, content) {
    return new Promise(function (resolve, reject) {
        fs.writeFile(`./test/${fileName}`, content, function (err, done) {
            if (err) {
                reject(err);
                return;
            }
            resolve(done);
            // console.log('file created >>', done);
        });
    })

}

function readFile(fileName, cb) {
    fs.readFile(`./test/${fileName}`, 'utf-8', function (err, done) {
        if (err) {
            cb(err);
            return;
        }
        cb(null, done);
    })
}

fs.rename(`./test/kisor.txt`, `./test/kisor1.txt`, function (err, done) {
    if (err) {
        console.log('rename failed');
        return;
    }
    console.log('renamed');
})

// to DO create function export it use inside server 
fs.unlink(`./test/abcd.js`, function (err, done) {
    if (err) {
        console.log('delete failed');
    } else {
        console.log('removed');
    }
})

// readFile('sdjklf',function(err,done){
//     if(err){
//         console.log("error in file reading >>",err);
//     }else{
//         console.log('success >>',done);
//     }
// })


// execution
// write('kisor.txt', 'i am javascript')
//     .then(function (data) {
//         console.log('write success ', data);
//     })
//     .catch(function (err) {
//         console.log('error in wirte >>', err);
//     })
// objec short hand
module.exports = {
    write, readFile,
    a: 'b',
    c: 'd',
    e: 'f'
};





